
/*
use the "require" directive to load Node.js modules

A "module" is a software component or part of a program that contains one or more routines

the "http module" let Node.js transfer data using Hyper Text Trasfer Protocol (http)
	it can create an HTTP server that listens to server ports such as...
	3000, 4000, 5000, 8000 (usually used for web development)
the "http module" is a set of individual files that contains code to create a "component" that helps establish data transfer between applications

Clients (devices/browsers) and server (nodeJS/expressJS applications) communicate by exchanging individual messages (requests/response)

Request - the messages sent by the clients

Response - the message sent by the server as response
*/

let http = require("http");

// using the module's createServer() method, we can create an http server that listens to request on a specified port that gives responses back to the client
// createServer() is a method of the http object responsible for creating a server using Node.js

http.createServer(function (request, response){

	// use writeHead() method to:
	// set a status code for the response - a 200 means ok
	// set the content-type of the response - as a plain text message
	response.writeHead(200, {'Content-Type': 'text/plain'});

	// send the response with the content 'Hello World'
	response.end('Hello World');

}).listen(4000);
// a port is a virtual point where network connections starts and ends
// the server will be assigned to port 4000 ia the listen() method
	// where the server will listen to any request that sent to it and will also send the response via this port

// when server is running, console will print a message:
console.log('Server is running at localhost: 4000');
// the messages or outputs from using console.log will now be displayed to the TERMINAL


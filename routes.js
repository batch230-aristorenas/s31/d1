
//use the "require" directive to load Node.js modules

// the "http module" let Node.js transfer data using Hyper Text Trasfer Protocol (http)
	//it can create an HTTP server that listens to server ports such as...
	//3000, 4000, 5000, 8000 (usually used for web development)
//the "http module" is a set of individual files that contains code to create a "component" that helps establish data transfer between applications
const http = require("http");

let port = 4000;

// using the module's createServer() method, we can create an http server that listens to request on a specified port that gives responses back to the client
// createServer() is a method of the http object responsible for creating a server using Node.js
const server = http.createServer((request, response) => {

	if(request.url == '/greeting'){
		// method of the response object that allows us to set status codes and content types
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end('Hello World');
	}
	else if(request.url == '/homepage'){
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end('You are now in the Homepage');
	}
	else{

		// set a status code for response - a 404 means not found
		response.writeHead(404, {'Content-Type': 'text/plain'});
		response.end('404 Page not available');
	}
	
})

server.listen(port)

console.log('Server now accessible at localhost ' + port);


